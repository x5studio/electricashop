<?


function p($data, $ignore = false){
    global $USER;

    if ($USER->IsAdmin() || $ignore) {
        echo "<pre>";
        print_r($data);
        echo "</pre>";
    }
}

function mb_ucfirst($text) {
    return mb_strtoupper(mb_substr($text, 0, 1)) . mb_substr($text, 1);
}

