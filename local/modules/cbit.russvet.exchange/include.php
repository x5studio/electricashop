<?php
IncludeModuleLangFile(__FILE__);

$moduleId = "cbit.russvet.exchange";

$arClasses = array(
    'Cbit\\Exchange\\eRequest' => 'lib/eRequest.php',
    'Cbit\\Exchange\\eHandlers' => 'lib/eHandlers.php',
    'Cbit\\Exchange\\eProduct' => 'lib/eProduct.php',
    'Cbit\\Exchange\\eSection' => 'lib/eSection.php',

    'Cbit\\Exchange\\eStocks' => 'lib/eStocks.php',
    'Cbit\\Exchange\\ePrices' => 'lib/ePrices.php',
    'Cbit\\Exchange\\eProperties' => 'lib/eProperties.php',
    'Cbit\\Exchange\\eResidues' => 'lib/eResidues.php',
);

\Bitrix\Main\Loader::registerAutoLoadClasses($moduleId, $arClasses);