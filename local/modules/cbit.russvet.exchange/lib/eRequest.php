<?php

namespace Cbit\Exchange;

class eRequest
{
    public static function sendRequest($action, $params = [])
    {
        $options = eHandlers::getModuleOptions();

        $username = $options['login'];
        $password = $options['password'];
        $url = rtrim($options['url'], '/').'/'.ltrim($action, '/');

        $headers = array(
            'Authorization: Basic ' . base64_encode($username . ':' . $password),
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

        $resultJson = curl_exec($ch);
        $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);   //get status code
        curl_close($ch);

        if ($status == 200) {
            $result = json_decode($resultJson, true);

            return $result;
        } else {
            // Throw error

            return null;
        }
    }
}