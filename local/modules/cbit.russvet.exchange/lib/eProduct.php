<?php

namespace Cbit\Exchange;

use Bitrix\Catalog\Model\Product;
use Bitrix\Catalog\ProductTable;
use Bitrix\Iblock\ElementTable;
use Bitrix\Main\Loader;

class eProduct
{
    static $moduleIblockIsIncluded = false;
    static $moduleCatalogIsIncluded = false;

    public static function getUploadStatus()
    {
        return eHandlers::getUploadStatus('product');
    }

    public static function setUploadStatus($value)
    {
        eHandlers::setUploadStatus('product', $value);
    }

    public static function getAction($stockXmlId, $category, $page)
    {
        return "/position/$stockXmlId/$category?page=$page&rows=10000";
    }

    public static function getExistingProductsIdByXmlId($onlyInstock = false)
    {
        if (!self::$moduleIblockIsIncluded) {
            Loader::includeModule("iblock");
            self::$moduleIblockIsIncluded = true;
        }

        $existingProductsIdByXmlId = [];

        $options = eHandlers::getModuleOptions();
        $iblockId = $options['iblock_id'];

        if ($onlyInstock) {
            $dbRes = \Bitrix\Iblock\Elements\ElementCatalogTable::getList([
                'select' => [
                    'ID', 'XML_ID', 'CATEGORY_2_' => 'CATEGORY_2',
                ],
                'filter' => [
                    "CATEGORY_2_VALUE" => 'instock',
                ],
            ]);
        } else {
            $dbRes = ElementTable::getList([
                'filter' => [
                    'IBLOCK_ID' => $iblockId,
                ],
                'select' => [
                    'ID', 'XML_ID',
                ],
            ]);
        }
        while ($arRes = $dbRes->fetch()) {
            $productXmlId = $arRes['XML_ID'];

            if ($productXmlId) {
                $existingProductsIdByXmlId[$productXmlId] = $arRes['ID'];
            }
        }

        return $existingProductsIdByXmlId;
    }

    public static function fillArFieldsAndArProps($newProduct, $category1, &$arFields, &$arProps)
    {
        $code = $newProduct['CODE'];
        $name = $newProduct['NAME'];
        $brand = $newProduct['BRAND'];
        $category2 = $newProduct['CATEGORY'];

        $arFields = [
            'NAME' => $name,
            'CODE' => \CUtil::translit($name, 'ru'),
            'XML_ID' => $code,
        ];

        $arProps = [
            'BRAND_STRING' => $brand,
            'CATEGORY' => $category2,
            'CATEGORY_2' => $category1,
        ];
    }

    public static function addProduct($productId)
    {
        if (!self::$moduleCatalogIsIncluded) {
            Loader::includeModule("catalog");
            self::$moduleCatalogIsIncluded = true;
        }

        Product::add([
            'ID' => $productId,
            'TYPE' => ProductTable::TYPE_PRODUCT,
        ]);
    }

    public static function uploadProducts()
    {
        if (self::getUploadStatus()) {
            return "";
        } else {
            self::setUploadStatus(1);
        }

        if (!self::$moduleIblockIsIncluded) {
            Loader::includeModule("iblock");
            self::$moduleIblockIsIncluded = true;
        }

        $options = eHandlers::getModuleOptions();
        $iblockId = $options['iblock_id'];

        $requiredStocksXmlIds = eStocks::getRequiredStocksXmlId();
        $existingProductsIdByXmlId = self::getExistingProductsIdByXmlId();

        $updatedProducts = [];
        $ibEl = new \CIBlockElement;

        $stockXmlId = reset($requiredStocksXmlIds);
        $category1 = 'instock';
        $action = self::getAction($stockXmlId, $category1, 1);
        $lastPage = eRequest::sendRequest($action)['meta']['last_page']+1;

        for ($page = 1; $page <= $lastPage; $page++) {
            $action = self::getAction($stockXmlId, $category1, $page);

            unset($requestResult);
            $requestResult = eRequest::sendRequest($action);

            unset($newProducts);
            $newProducts = $requestResult['items'];
            $newProducts = reset($newProducts);

            foreach ($newProducts as $newProduct) {
                $productXmlId = $newProduct['CODE'];
                $productId = $existingProductsIdByXmlId[$productXmlId];

                if ($updatedProducts[$productXmlId] !== true) {
                    $updatedProducts[$productXmlId] = true;

                    $arFields = [];
                    $arProps = [];
                    $tmpFilesToRemove = [];

                    self::fillArFieldsAndArProps($newProduct, $category1, $arFields, $arProps);
                    eProperties::fillArFieldsAndArProps($productXmlId, $arFields, $arProps, $tmpFilesToRemove);

                    if ($productId > 0) {
                        // Update product

                        $ibEl->Update($productId, $arFields);
                    } else {
                        // Add product

                        $arFields['IBLOCK_ID'] = $iblockId;

                        $existingProductsIdByXmlId[$productXmlId] = $ibEl->Add($arFields);
                        $productId = $existingProductsIdByXmlId[$productXmlId];

                        self::addProduct($productId);
                    }

                    \CIBlockElement::SetPropertyValuesEx($productId, $iblockId, $arProps);

                    foreach ($tmpFilesToRemove as $tmpFile) {
                        unlink($tmpFile);
                    }
                }
            }
        }

        self::setUploadStatus(0);
        \CAgent::AddAgent('\Cbit\Exchange\ePrices::uploadPrices();', 'cbit.russvet.exchange');

        return "";
    }
}