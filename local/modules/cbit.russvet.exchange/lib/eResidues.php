<?php

namespace Cbit\Exchange;

use Bitrix\Catalog\MeasureTable;
use Bitrix\Catalog\Model\Product;
use Bitrix\Catalog\StoreProductTable;
use Bitrix\Main\Loader;

class eResidues
{
    const ACTION = '/residue/<stock_id>/<product_id>';
    static $moduleCatalogIsIncluded = false;

    public static function getExistingResiduesIdByProductId()
    {
        static $cache = null;

        if (is_null($cache)) {
            if (!self::$moduleCatalogIsIncluded) {
                Loader::includeModule("catalog");
                self::$moduleCatalogIsIncluded = true;
            }

            $cache = [];

            $dbRes = StoreProductTable::getList([
                'select' => [
                    'ID', 'STORE_ID', 'PRODUCT_ID',
                ],
            ]);
            while ($arRes = $dbRes->fetch()) {
                $cache[$arRes['PRODUCT_ID']][$arRes['STORE_ID']] = $arRes['ID'];
            }
        }

        return $cache;
    }

    public static function getExistingUoms()
    {
        static $cache = null;

        if (is_null($cache)) {
            if (!self::$moduleCatalogIsIncluded) {
                Loader::includeModule("catalog");
                self::$moduleCatalogIsIncluded = true;
            }

            $cache = [];

            // API D7 работает неверно - возвращает пустой MEASURE_TITLE
            $dbRes = \CCatalogMeasure::getList();
            while ($arRes = $dbRes->Fetch()) {
                $cache[$arRes['MEASURE_TITLE']] = $arRes['ID'];
            }
        }

        return $cache;
    }

    public static function prepareMeasureId($uom)
    {
        static $cache = null;

        if (is_null($cache)) {
            $cache = self::getExistingUoms();
        }

        $measureId = null;

        if (is_string($uom) && $uom!=="") {
            $uom = trim($uom);
            $uom = rtrim($uom, '.');
            $uom = rtrim($uom);
            $uom = mb_strtolower($uom);
            $uom = mb_ucfirst($uom);

            if ($cache[$uom] > 0) {
                // Measure exists

                $measureId = $cache[$uom];
            } else {
                // Add new measure

                $addResult = MeasureTable::add([
                    'CODE' => random_int(10000, 1000000),
                    'MEASURE_TITLE' => $uom,
                    'SYMBOL_RUS' => $uom,
                    'SYMBOL_INTL' => $uom,
                    'SYMBOL' => $uom,
                ]);

                $measureId = $addResult->getId();

                $cache[$uom] = $measureId;
            }
        }

        return $measureId;
    }

    public static function setTotalResiduesAndUomForOneProduct($productId, $totalResidues, $uom)
    {
        if (!self::$moduleCatalogIsIncluded) {
            Loader::includeModule("catalog");
            self::$moduleCatalogIsIncluded = true;
        }

        $arFields = [
            'QUANTITY' => $totalResidues,
        ];

        $measureId = self::prepareMeasureId($uom);
        if ($measureId > 0) {
            $arFields['MEASURE'] = $measureId;
        }

        Product::update($productId, $arFields);
    }

    public static function getExistingResidueId($productId, $stockId)
    {
        $existingResiduesIdByProductId = self::getExistingResiduesIdByProductId();

        $residueId = $existingResiduesIdByProductId[$productId][$stockId];

        return $residueId;
    }

    public static function getResidueFromRussvet($stockXmlId, $productXmlId)
    {
        $action = self::ACTION;
        $action = str_replace(['<stock_id>','<product_id>'], [$stockXmlId, $productXmlId], $action);

        $requestResult = eRequest::sendRequest($action);

        return $requestResult;
    }

    public static function uploadResidueForOneProductInOneStock($productId, $stockId, $newResidue)
    {
        if (!self::$moduleCatalogIsIncluded) {
            Loader::includeModule("catalog");
            self::$moduleCatalogIsIncluded = true;
        }

        if (!is_null($newResidue)) {
            $existingResidueId = self::getExistingResidueId($productId, $stockId);

            if ($existingResidueId) {
                // Update residues

                StoreProductTable::Update(
                    $existingResidueId,
                    [
                        'AMOUNT' => $newResidue,
                    ]
                );
            } else {
                // Add residues

                StoreProductTable::Add([
                    'STORE_ID' => $stockId,
                    'PRODUCT_ID' => $productId,
                    'AMOUNT' => $newResidue,
                ]);
            }
        } else {
            // Request error - do not update residues
        }

        return $newResidue;
    }

    public static function uploadResidues()
    {
        $existingStocksIdByXmlId = eStocks::getExistingStocksIdByXmlId();
        $existingProductsIdByXmlId = eProduct::getExistingProductsIdByXmlId(true);

        // Цикл по товарам
        foreach ($existingProductsIdByXmlId as $productXmlId => $productId) {
            $totalResidues = 0;

            // Цикл по складам
            foreach ($existingStocksIdByXmlId as $stockXmlId => $stockId) {
                $residueRequestResult = self::getResidueFromRussvet($stockXmlId, $productXmlId);
                $newResidue = $residueRequestResult['Residue'];
                $uom = $residueRequestResult['UOM'];

                $totalResidues += self::uploadResidueForOneProductInOneStock($productId, $stockId, $newResidue);
            }

            self::setTotalResiduesAndUomForOneProduct($productId, $totalResidues, $uom);
        }

        return "";
    }
}