<?php

namespace Cbit\Exchange;

use Bitrix\Main\Config\Option;

class eHandlers
{
    const moduleId = "cbit.russvet.exchange";

    public static function getModuleOptions()
    {
        static $options = null;

        if (is_null($options)) {
            $arOptions = Option::get(self::moduleId, "settings");
            $options = unserialize($arOptions);
        }

        return $options;
    }

    public static function getUploadStatus($name)
    {
        return Option::get(self::moduleId, $name."_upload_status");
    }

    public static function setUploadStatus($name, $value)
    {
        Option::set(self::moduleId, $name."_upload_status", $value);
    }

    public static function fullExchangeAgent()
    {
        \CAgent::AddAgent('\Cbit\Exchange\eProduct::uploadProducts();', 'cbit.russvet.exchange');

        return '\\'.__METHOD__.'();';
    }

}