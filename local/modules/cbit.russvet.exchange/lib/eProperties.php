<?php

namespace Cbit\Exchange;

use Bitrix\Iblock\SectionTable;
use Bitrix\Main\Loader;

class eProperties
{
    const ACTION = '/specs/<product_id>';
    const SECTIONS_CACHE_IDENTIFIER = 'catalog_sections';
    static $moduleIblockIsIncluded = false;

    public static function downloadFileByUrl($url, $newFileDirAbsPath)
    {
        $parts = explode('/', $url);
        $fileName = end($parts);
        $fileAbsPath = $newFileDirAbsPath.$fileName;
        file_put_contents($fileAbsPath, fopen($url, 'r'));

        return $fileAbsPath;
    }

    public static function getExistingSectionsIdByXmlId()
    {
        $existingSectionsIdByXmlId = [];

        if (!self::$moduleIblockIsIncluded) {
            Loader::includeModule("iblock");
            self::$moduleIblockIsIncluded = true;
        }

        $options = eHandlers::getModuleOptions();
        $iblockId = $options['iblock_id'];

        $dbRes = SectionTable::getList([
            'filter' => [
                'IBLOCK_ID' => $iblockId,
            ],
        ]);
        while ($arRes = $dbRes->fetch()) {
            $existingSectionsIdByXmlId[$arRes['XML_ID']] = $arRes['ID'];
        }

        return $existingSectionsIdByXmlId;
    }

    public static function getSectionIdByXmlId($xmlId, $name = '', $parent = 0)
    {
        $cache = new \CPHPCache();
        if ($cache->InitCache(86400, self::SECTIONS_CACHE_IDENTIFIER, '/')) {
            $res = $cache->GetVars();
            $existingSectionsIdByXmlId = $res[self::SECTIONS_CACHE_IDENTIFIER];
        } elseif ($cache->StartDataCache()) {
            $existingSectionsIdByXmlId = self::getExistingSectionsIdByXmlId();

            $cache->EndDataCache([self::SECTIONS_CACHE_IDENTIFIER => $existingSectionsIdByXmlId]);
        } else {
            $existingSectionsIdByXmlId = self::getExistingSectionsIdByXmlId();
        }

        $sectionId = $existingSectionsIdByXmlId[$xmlId];

        if (is_null($sectionId)) {
            if (!self::$moduleIblockIsIncluded) {
                Loader::includeModule("iblock");
                self::$moduleIblockIsIncluded = true;
            }

            $options = eHandlers::getModuleOptions();
            $iblockId = $options['iblock_id'];

            $ibSec = new \CIBlockSection;
            $sectionId = $ibSec->Add([
                'IBLOCK_ID' => $iblockId,
                'NAME' => $name,
                'CODE' => \CUtil::translit($name, 'ru'),
                'XML_ID' => $xmlId,
                'IBLOCK_SECTION_ID' => $parent,
            ]);

            $cache->CleanDir();
        }

        return $sectionId;
    }

    public static function getPropsFromRussvet($productXmlId)
    {
        $action = self::ACTION;
        $action = str_replace('<product_id>', $productXmlId, $action);

        $requestResult = eRequest::sendRequest($action);

        return $requestResult;
    }

    public static function fillArFieldsAndArProps($productXmlId, &$arFields, &$arProps, &$tmpFilesToRemove)
    {
        $requestResult = self::getPropsFromRussvet($productXmlId);

        if ($requestResult) {
            $tmpDir = $_SERVER['DOCUMENT_ROOT'].'/upload/tmp_russvet_exchange/';
            if (!is_dir($tmpDir)) {
                mkdir($tmpDir);
            }

            $info = $requestResult['info'];
            foreach ($info as $infoItem) {
                $arFields['PREVIEW_TEXT'] = $infoItem['DESCRIPTION'];
                $arFields['DETAIL_TEXT'] = $infoItem['DESCRIPTION'];

                $arProps['PRIMARY_UOM'] = $infoItem['PRIMARY_UOM'];
                $arProps['MULTIPLICITY'] = $infoItem['MULTIPLICITY'];
                $arProps['ITEMS_PER_UNIT'] = $infoItem['ITEMS_PER_UNIT'];
                $arProps['VENDOR_CODE'] = $infoItem['VENDOR_CODE'];
                $arProps['SERIES'] = $infoItem['SERIES'];
                $arProps['PROP_2084'] = $infoItem['ORIGIN_COUNTRY'];
                $arProps['PROP_2120'] = $infoItem['WARRANTY'];

                $parentSectionId = self::getSectionIdByXmlId($infoItem['ETIM_GROUP'], $infoItem['ETIM_GROUP_NAME']);
                $sectionId = self::getSectionIdByXmlId($infoItem['ETIM_CLASS'], $infoItem['ETIM_CLASS_NAME'], $parentSectionId);
                $arFields['IBLOCK_SECTION_ID'] = $sectionId;
            }

            $barcode = $requestResult['barcode'];
            foreach ($barcode as $barcodeItem) {
                $arProps['BARCODE'][] = [
                    'VALUE' => $barcodeItem['EAN'],
                    'DESCRIPTION' => $barcodeItem['DESCRIPTION'],
                ];
            }

            $specs = $requestResult['specs'];
            foreach ($specs as $specsItem) {
                $specName = $specsItem['NAME'];
                $specValue = $specsItem['VALUE'];
                $specUom = $specsItem['UOM'];

                if ($specUom) {
                    $specName .= ', ' . $specUom;
                }

                $arProps['SPECS'][] = [
                    'VALUE' => $specValue,
                    'DESCRIPTION' => $specName,
                ];
            }

            $img = $requestResult['img'];

            $mainImg = array_shift($img);
            $imgUrl = $mainImg['URL'];

            $imgAbsTmpPath = self::downloadFileByUrl($imgUrl, $tmpDir);
            $tmpFilesToRemove[] = $imgAbsTmpPath;

            $arFields['PREVIEW_PICTURE'] = \CFile::MakeFileArray($imgAbsTmpPath);
            $arFields['DETAIL_PICTURE'] = \CFile::MakeFileArray($imgAbsTmpPath);

            foreach ($img as $imgItem) {
                $imgUrl = $imgItem['URL'];
                $imgAbsTmpPath = self::downloadFileByUrl($imgUrl, $tmpDir);
                $tmpFilesToRemove[] = $imgAbsTmpPath;

                $arProps['MORE_PHOTO'][] = \CFile::MakeFileArray($imgAbsTmpPath);
            }

            return true;
        } else {
            // Request error - do not update props

            return false;
        }
    }

    public static function uploadProperties()
    {
        if (!self::$moduleIblockIsIncluded) {
            Loader::includeModule("iblock");
            self::$moduleIblockIsIncluded = true;
        }

        $options = eHandlers::getModuleOptions();
        $iblockId = $options['iblock_id'];

        $existingProductsIdByXmlId = eProduct::getExistingProductsIdByXmlId();

        foreach ($existingProductsIdByXmlId as $productXmlId => $productId) {
            $arFields = [];
            $arProps = [];
            $tmpFilesToRemove = [];

            $success = self::fillArFieldsAndArProps($productXmlId, $arFields, $arProps, $tmpFilesToRemove);

            if ($success) {
                $ibEl = new \CIBlockElement;
                $ibEl->Update($productId, $arFields);

                \CIBlockElement::SetPropertyValuesEx($productId, $iblockId, $arProps);

                foreach ($tmpFilesToRemove as $tmpFile) {
                    unlink($tmpFile);
                }
            } else {
                // Request error - do not update props
            }
        }

        return "";
    }
}