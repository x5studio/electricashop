<?php

namespace Cbit\Exchange;

use Bitrix\Catalog\GroupTable;
use Bitrix\Catalog\PriceTable;
use Bitrix\Main\Loader;

class ePrices
{
    const ACTION = '/price/<product_id>';
    static $moduleCatalogIsIncluded = false;

    public static function getExistingPriceTypeNameById($flip = false)
    {
        static $cache = [];

        $key = ($flip ? 'flipped' : 'unflipped');

        if (!array_key_exists($key, $cache)) {
            if (!self::$moduleCatalogIsIncluded) {
                Loader::includeModule("catalog");
                self::$moduleCatalogIsIncluded = true;
            }

            $cache[$key] = [];

            $dbRes = GroupTable::getList();
            while ($arRes = $dbRes->fetch()) {
                if ($flip) {
                    $cache[$key][$arRes['NAME']] = $arRes['ID'];
                } else {
                    $cache[$key][$arRes['ID']] = $arRes['NAME'];
                }
            }
        }

        return $cache[$key];
    }

    public static function getExistingPricesIdByProductId()
    {
        static $cache = null;

        if (is_null($cache)) {
            if (!self::$moduleCatalogIsIncluded) {
                Loader::includeModule("catalog");
                self::$moduleCatalogIsIncluded = true;
            }

            $cache = [];

            $existingPriceTypeNameById = self::getExistingPriceTypeNameById();

            $dbRes = PriceTable::getList([
                'select' => [
                    'ID', 'CATALOG_GROUP_ID', 'PRODUCT_ID',
                ],
            ]);
            while ($arRes = $dbRes->fetch()) {
                $priceTypeName = $existingPriceTypeNameById[$arRes['CATALOG_GROUP_ID']];

                if ($priceTypeName) {
                    $cache[$arRes['PRODUCT_ID']][$priceTypeName] = $arRes['ID'];
                }
            }
        }

        return $cache;
    }

    public static function getExistingPriceId($productId, $priceTypeCode)
    {
        $existingPricesIdByProductId = self::getExistingPricesIdByProductId();

        $priceId = $existingPricesIdByProductId[$productId][$priceTypeCode];

        return $priceId;
    }

    public static function getPriceFromRussvet($productXmlId)
    {
        $action = self::ACTION;
        $action = str_replace('<product_id>', $productXmlId, $action);

        $requestResult = eRequest::sendRequest($action);
        $newPrices = $requestResult['Price'];

        return $newPrices;
    }

    public static function setPrice($productId, $newPriceCode, $newPriceValue)
    {
        if (!self::$moduleCatalogIsIncluded) {
            Loader::includeModule("catalog");
            self::$moduleCatalogIsIncluded = true;
        }

        $existingPriceTypeIdByName = self::getExistingPriceTypeNameById(true);

        $realPriceCode = ($newPriceCode == 'Retail' ? 'BASE' : $newPriceCode);

        $existingPriceId = self::getExistingPriceId($productId, $realPriceCode);

        if ($existingPriceId) {
            // Update price

            PriceTable::Update(
                $existingPriceId,
                [
                    'PRICE' => $newPriceValue,
                    'PRICE_SCALE' => $newPriceValue,
                ]
            );
        } elseif ($productId) {
            // Add price

            PriceTable::Add([
                'CATALOG_GROUP_ID' => $existingPriceTypeIdByName[$realPriceCode],
                'PRODUCT_ID' => $productId,
                'PRICE' => $newPriceValue,
                'PRICE_SCALE' => $newPriceValue,
                'CURRENCY' => 'RUB',
            ]);
        }
    }

    public static function uploadAllPricesForOneProduct($productXmlId, $productId)
    {
        $newPrices = self::getPriceFromRussvet($productXmlId);

        // Цикл по типам цены
        foreach ($newPrices as $newPriceCode => $newPriceValue) {
            self::setPrice($productId, $newPriceCode, $newPriceValue);
        }
    }

    public static function uploadPrices()
    {
        $existingProductsIdByXmlId = eProduct::getExistingProductsIdByXmlId();

        // Цикл по товарам
        foreach ($existingProductsIdByXmlId as $productXmlId => $productId) {
            self::uploadAllPricesForOneProduct($productXmlId, $productId);
        }

        \CAgent::AddAgent('\Cbit\Exchange\eResidues::uploadResidues();', 'cbit.russvet.exchange');

        return "";
    }
}