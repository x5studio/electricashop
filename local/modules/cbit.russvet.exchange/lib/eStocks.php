<?php

namespace Cbit\Exchange;

use Bitrix\Catalog\StoreTable;
use Bitrix\Main\Loader;

class eStocks
{
    const ACTION = '/stocks';
    static $moduleCatalogIsIncluded = false;

    public static function getRequiredStocksXmlId()
    {
        return [
            96, // Тверь
        ];
    }

    public static function getExistingStocksIdByXmlId()
    {
        static $cache = null;

        if (is_null($cache)) {
            if (!self::$moduleCatalogIsIncluded) {
                Loader::includeModule("catalog");
                self::$moduleCatalogIsIncluded = true;
            }

            $cache = [];

            $dbRes = StoreTable::getList();
            while ($arRes = $dbRes->fetch()) {
                $cache[$arRes['XML_ID']] = $arRes['ID'];
            }
        }

        return $cache;
    }

    public static function uploadStocks()
    {
        if (!self::$moduleCatalogIsIncluded) {
            Loader::includeModule("catalog");
            self::$moduleCatalogIsIncluded = true;
        }

        $requestResult = eRequest::sendRequest(self::ACTION);
        $newStocks = $requestResult['Stocks'];

        if ($newStocks) {
            $existingStocksIdByXmlId = self::getExistingStocksIdByXmlId();

            foreach ($newStocks as $stock) {
                $code = $stock['ORGANIZATION_ID'];
                $name = $stock['NAME'];

                if (in_array($code, array_keys($existingStocksIdByXmlId))) {
                    // Update stock name and address

                    StoreTable::update(
                        $existingStocksIdByXmlId[$code],
                        [
                            'TITLE' => $name,
                            'ADDRESS' => $name,
                        ]
                    );
                } elseif (in_array($code, self::getRequiredStocksXmlId())) {
                    // Add new stock

                    StoreTable::add([
                        'SITE_ID' => 's1',
                        'TITLE' => $name,
                        'ADDRESS' => $name,
                        'XML_ID' => $code,
                    ]);
                }
            }
        }

        \CAgent::AddAgent('\Cbit\Exchange\eProduct::uploadProducts();', 'cbit.russvet.exchange');

        return "";
    }
}